package com.FirstLab;

import java.util.Objects;

public class UniversityFullAddress {
    private String address;
    private String city;
    private int index;

    public UniversityFullAddress() {

    }
    //везде нужно обращаться через свойтсва?
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
       this.index = index;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public UniversityFullAddress(String address, String city, int index) {
        setAddress(address);
        setCity(city);
        setIndex(index);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UniversityFullAddress that = (UniversityFullAddress) o;
        return getIndex() == that.getIndex() &&
                Objects.equals(getAddress(), that.getAddress()) &&
                Objects.equals(getCity(), that.getCity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAddress(), getCity(), getIndex());
    }

    @Override
    //везде нужно обращаться через свойтсва?
    public String toString() {
        return
                '\'' + getAddress() + '\'' +
                ", city='" + getCity() + '\'' +
                ", index=" + getIndex();
    }
}
