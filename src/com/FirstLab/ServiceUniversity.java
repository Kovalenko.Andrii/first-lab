package com.FirstLab;
import java.util.ArrayList;
import java.util.List;
import java.util.List;
import java.util.Objects;

public class ServiceUniversity {
    private List<University> servListUniversity;

    public List<University> getListUniversity() {
        return servListUniversity;
    }
    public void setListUniversity(List<University> listUniversity){
        servListUniversity = listUniversity;
    }
    public ServiceUniversity(List<University> listUniversity) {
        setListUniversity(listUniversity);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServiceUniversity that = (ServiceUniversity) o;
        return Objects.equals(servListUniversity, that.servListUniversity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(servListUniversity);
    }

    public ArrayList<University> UniversitiesInTheCity(String cityFind){
        ArrayList<University> universitiesInTheCity = new ArrayList<University>();
        for(int i = 0; i < getListUniversity().size() ; i++){
            if(getListUniversity().get(i).getAddress().getCity().equals(cityFind)){
                universitiesInTheCity.add(getListUniversity().get(i));
            }
        }
        return universitiesInTheCity;
    }
    public void ShowUniversitiesAccreditationLevel (accreditationLevel findAccreditationLevel){
        System.out.printf("Universities with the same level of accreditation as: %s",findAccreditationLevel);
        for(int i = 0; i < getListUniversity().size() ; i++){
            if(getListUniversity().get(i).getLevel().equals(findAccreditationLevel)){
                System.out.println(getListUniversity().get(i).getName());
            }
        }
    }
}
