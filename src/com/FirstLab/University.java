package com.FirstLab;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class University {
    private String name;
    private UniversityFullAddress address;
    private int yearOfEstablishment;
    accreditationLevel level;

    @Override
    public String toString() {
        return new StringBuilder().append("University{").append("name='").append(getName()).append('\'').append(", address=").append(getAddress()).append(", yearOfEstablishment=").append(getYearOfEstablishment()).append(", level=").append(getLevel()).append('}').toString();
    }

    public int getYearOfEstablishment() {
        return yearOfEstablishment;
    }

    public void setYearOfEstablishment(int yearOfEstablishment) {
        this.yearOfEstablishment = yearOfEstablishment;
    }

    public accreditationLevel getLevel() { return level; }

    public void setLevel(accreditationLevel level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UniversityFullAddress getAddress() {
        return address;
    }

    public void setAddress(UniversityFullAddress address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        University that = (University) o;
        return getYearOfEstablishment() == that.getYearOfEstablishment() &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getAddress(), that.getAddress()) &&
                getLevel() == that.getLevel();
    }
    @Override
    public int hashCode() {
        return Objects.hash(getName(), getAddress(), getYearOfEstablishment(), getLevel());
    }


    public University(String name, UniversityFullAddress address, int yearOfEstablishment, accreditationLevel level) {
        this.setName(name);
        this.setAddress(address);
        this.setYearOfEstablishment(yearOfEstablishment);
        this.setLevel(level);
    }

}
