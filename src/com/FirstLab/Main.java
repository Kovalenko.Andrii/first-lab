package com.FirstLab;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Main {

    public static void main(String[] args) {
        List<University> universitiesList = new ArrayList<University>();
        universitiesList.add(
                new University("Киевский международный университет (КиМУ)",
                        new UniversityFullAddress("ул. Львовская, 49","Киев",03134),
                        1967,
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Азовский морской институт Одесской национальной морской академии (АМИ ОНМА)",
                        new UniversityFullAddress("ул. Черноморская 19","Мариуполь",87500),
                        1979,
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Академия внутренних войск МВД Украины (АВВУ)",
                        new UniversityFullAddress("пл. Восстания 3","Харьков",61000),
                        1989,
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Винницкий национальный аграрный университет (ВНАУ)",
                        new UniversityFullAddress("ул. Солнечная 3","Винница",21000),
                        1988,
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Донецкий университет экономики и права (ДонУЭП)",
                        new UniversityFullAddress("ул. Университетская 77","Донецк",83000),
                        1979,
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Житомирский государственный университет им. И. Франко (ЖГУ)",
                        new UniversityFullAddress("ул. Б. Бердичевская 40","Житомир",10000),
                        1983,
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Запорожский национальный университет (ЗНУ)",
                        new UniversityFullAddress("ул. Жуковского 66","Запорожье",69000),
                        1978,
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Криворожский педагогический институт КНУ",
                        new UniversityFullAddress("пр. Гагарина 54","Кривой Рог",50000),
                        1978,
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Львовский национальный медицинский университет им. Д. Галицкого (ЛНМУ)",
                        new UniversityFullAddress("ул. Пекарская 69","Львов",79000),
                        1966,
                        accreditationLevel.IV));
        universitiesList.add(
                new University("Международный Славянский университет",
                        new UniversityFullAddress("ул. О. Яроша 9-А","Харьков",61000),
                        1993,
                        accreditationLevel.III));

        ServiceUniversity serviceUniversity;
        serviceUniversity = new ServiceUniversity(universitiesList);

        List<University> universityInTheCity ;
        String cityFind = "Киев";

        universityInTheCity = serviceUniversity.UniversitiesInTheCity(cityFind);
        for(int i = 0; i < universityInTheCity.size();i++){
            System.out.println(universityInTheCity.get(i).toString());
        }
        serviceUniversity.ShowUniversitiesAccreditationLevel(accreditationLevel.IV);
    }
}
